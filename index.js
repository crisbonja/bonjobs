const express = require('express');
const app = express();
const path = require('path');
const port = process.env.PORT || 3000;

const sqlite = require('sqlite');

const dbConnection = sqlite.open(path.resolve(__dirname, 'banco.sqlite'), {Promise});

const bodyParser = require('body-parser');


app.set('views', path.join(__dirname, 'views'));
app.set('public', path.join(__dirname, 'public'));

// app.use('/admin', (req, res, next) => {
//   if(req.hostname === 'localhost') {
//     next();
//   } else {
//     res.send('Not Allowed!');
//   }
// });

app.set('view engine', 'ejs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', async (request, response) => {
  const db = await dbConnection;
  const categoriasDB = await db.all('select * from categorias'); 
  const vagas = await db.all('select * from vagas');
  const categorias = categoriasDB.map(cat => {
    return {
      ...cat,
      vagas: vagas.filter(vaga => vaga.categoria === cat.id)
    }
  })
  response.render('home', {
    categorias
  });
});

app.get('/vaga/:id', async (request, response) => {  
  //step 3 visualizando a rota com vaga do banco
  const db = await dbConnection // criando conexao
  const vaga = await db.get('select * from vagas where id=' +request.params.id); // passando a rota
  response.render('vaga', { vaga })
});

// criando a rota de admin
app.get('/admin', (req, res) => {
  res.render('admin/home');
});

// criando a rota de vagas
app.get('/admin/vagas', async (req, res) => {
  const db = await dbConnection;
  // selecionando todas as vagas para renderizar na rota de admin
  const vagas = await db.all('select * from vagas'); 
  // console.log(vagas);
  // passando a rota 
  res.render('admin/vagas', { vagas });
});

//excluindo a rota de vagas
app.get('/admin/vagas/delete/:id', async (req, res) => {
  const db = await dbConnection;
  await db.run('delete from vagas where id=' + req.params.id+ '');
  res.redirect('/admin/vagas');
});

// create new vaga
app.get('/admin/vagas/nova', async(req, res) => {
  const db = await dbConnection;
  const categorias = await db.all('select * from categorias');
  res.render('admin/criar-vaga', { categorias });
});

app.post('/admin/vagas/nova', async(req, res) => {
  const { titulo, descricao, categoria } = req.body; 
  const db = await dbConnection;  
  await db.run(`insert into vagas(categoria, titulo, descricao) values ('${categoria}', '${titulo}', '${descricao}')`);
  res.redirect('/admin/vagas');
});

// promise para inicializar o banco e criar minha tabela categorias
const init = async() => {
  // abrindo conexão
  const db = await dbConnection; 
  // criando a tabela caso não exista com duas colunas para categorias
  await db.run('create table if not exists categorias (id INTEGER PRIMARY KEY, categoria TEXT);');
  // atribuir a minha categoria
   // const categoria = 'Recursos Humanos';
   //await db.run(`insert into categorias(categoria) values ('${categoria}')`);
  await db.run('create table if not exists vagas (id INTEGER PRIMARY KEY, categoria INTEGER, titulo TEXT, descricao TEXT);');
  //  const vaga = 'Social Media';
  // const descricao = `Create social media to Facebook, Instagram and others Pages`
  // await db.run(`insert into vagas(categoria, titulo, descricao) values (2,'${vaga}', '${descricao}')`);
}

init();

app.listen(port, (error) => {
  if(error) {
    console.log('Não foi possivel conectar no Servidor =(');
  } else {
    console.log('App rodando!');
  }
});